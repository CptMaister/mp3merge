#!/usr/bin/env python3 
# -*- coding: utf-8 -*-

import getopt, sys, os
from pathlib import Path
import subprocess

""" This script merges mp3 files, with batch file processing capabilities.

This shell script was the basis for creating this python script:
ffmpeg -i "concat:`ls *_01*.mp3 | xargs echo | tr ' ' '|'`" -acodec copy 01.mp3
It did a good job merging all mp3-files with '_01' anywhere in the file name to the output file 01.mp3, so all dialogue lines belonging to episode 1 of a series in my directory, but this python script realizes running one script for all 30+ episodes at once.
Like in the shell script, we use ffmpeg to do the job in the end.
"""

##### Methods #####

def get_adjusted_path(path : str) -> str:
    """Adjust a path string.
    
    Doesn't do much, currently only adds a / if missing at the end.
    """
    if path[-1] == "/":
        return path
    else:
        return path + "/"


def get_files_ffmpeg_episode(episode_string : str) -> str:
    """Create a string for ffmpeg concat with all files to merge.
    
    Takes a string that is supposed to only be present in all the files that are supposed to end up in one merged file and returns all the files in the source_path that contain the string, joined with | for ffmpeg, and an output filepath.
    First we copy all filenames to a new list, that contain the episode_string.
    Then return all these as one string, joined with "|".
    """
    global ls_lines
    filtered_lines = []
    for line in ls_lines:
        if episode_string in line:
            filtered_lines.append(source_path + line)

    return "|".join(filtered_lines)


def get_output_name_path(episode_string="") -> str:
    """Get output name and file path of merged file.

    Uses the input from the name option, if used.
    """
    global output_name
    if output_name == "":
        output_name = source_path.replace("/", "\n").splitlines()[-1]
    elif episode_string == "":
        return output_path + output_name + '.mp3'

    if episode_string == "":
        return output_path + 'merged-' + output_name + '-etc.mp3'
    else:
        return output_path + output_name + episode_string.replace('.', '') + '.mp3'


def get_help_string(): #TODO
    return """Help text

    Usage: mp3merge [<options>]
       or: mp3merge [<options>] FILE1 FILE2 ... FILEn
       or: mp3merge [<options>] --episodes [<options>] EP_STR1 EP_STR2 ... EP_STRn
    """




##### Script Start #####

merge_strings = []
output_name = ""
output_path = get_adjusted_path(os.getcwd()) + "mp3merged-files/"
source_path = get_adjusted_path(os.getcwd())
help_string = get_help_string()
episode_merge_mode = False
overwrite_flag = '-y'

### handle arguments and argument options with getopt and sys
sys_args = sys.argv.copy()
argument_list = sys_args[1:]

short_options = "hen:o:s:"
long_options = ["help", "episodes", "name=", "output=", "source=", "no_overwrite"]

# parse arguments using getopt
try:
    options, arguments = getopt.getopt(argument_list, short_options, long_options)
except getopt.error as err:
    # Output error, and return with an error code
    print (str(err))
    sys.exit(2)

if arguments:
    merge_strings = arguments.copy()

## handle argument options
if "-h" in sys_args or "--help" in sys_args:
        print (help_string)
        exit()

# find all passed options
all_options = []
for option, value in options:
    all_options.append(option)

# act on options
for current_option, current_value in options:
    if current_option in ("-h", "--help"):
        print (help_string)
        exit()
    elif current_option in ("-e", "--episodes"):
        # All non-options arguments will be handled as strings that determine which files to merge together. For every one of these arguments, this script will look for all files that contain the argument in their name and merge them together.
        if arguments:
            print (" Option --episodes passed. Running in episodes mode.")
            episode_merge_mode = True
        else:
            print(" Option --episodes passed, but no non-option arguments. Invalid call, end prorgamm. Pass --help to see the manual.")
            exit()
    elif current_option in ("-n", "--name"):
        output_name = current_value
        print ((" Option --name passed. Setting output name to (%s), the merged file will have this name. If option --episodes is used, then all merged files will have this at the beginning of their name.") % (output_name))
    elif current_option in ("-o", "--output"):
        output_path = get_adjusted_path(current_value)
        print ((" Option --output passed. Setting output to (%s), all created files will go there.") % (output_path))
    elif current_option in ("-s", "--source"):
        # Setting source where the files to be merged are located to (%s). This option will be ignored if non-option arguments are passed, except when option --episodes is passed. Then only episode audios that are located in (%s) will be merged.
        source_path = get_adjusted_path(current_value)
        if (not "-e" in options) and (not "--episodes" in options):
            if arguments:
                print (" Option --source passed. But it is ignored because the files to be merged are explicitly passed as arguments.")
            else:
                print ((" Option --source passed. Setting source where the files to be merged are located to (%s).") % (source_path))
    elif current_option in "--no_overwrite":
        overwrite_flag = '-n'
        print (" Option --no_overwrite passed. No files will be overwritten.")
        


## read all filenames from the source_path and create a string list from it, then filter to keep only ones with ".mp3" at the end
ls_string = subprocess.check_output(['ls', source_path], universal_newlines=True)
ls_lines_unfiltered = ls_string.splitlines()
ls_lines = []
for line in ls_lines_unfiltered:
    if line[-4:] == ".mp3":
        ls_lines.append(line)


## safely create output directory if not existing, else do nothing
Path(output_path[0:-1]).mkdir(parents=True, exist_ok=True)


## run terminal ffmpeg command with all merge_strings, if not empty
if merge_strings and episode_merge_mode:
    # episode_merge_mode is on, handle arguments as episode filename components
    print(" Episode strings: ", merge_strings)
    for merge_string in merge_strings:
        files_ffmpeg_episode = get_files_ffmpeg_episode(merge_string)
        if files_ffmpeg_episode == "":
            print(("No files found with (%s) in their name.") % (merge_string))
        else:
            subprocess.run(['ffmpeg', '-i', 'concat:' + files_ffmpeg_episode, '-acodec', 'copy', overwrite_flag, get_output_name_path(merge_string)])
    exit()
elif merge_strings:
    # Arguments were passed, but not in episode_merge_mode, so just merge the files from the arguments together
    print(" Merging files together: ", merge_strings)
    subprocess.run(['ffmpeg', '-i', 'concat:' + "|".join(merge_strings), '-acodec', 'copy', overwrite_flag, output_path + 'merged-' + merge_strings[0][:-4] + '-etc.mp3'])
    exit()
else:
    # no arguments, no episode_merge_mode, just merge all mp3 files in the source_path
    if ls_lines:
        print((" Merging all mp3-files in (%s) together.") % (source_path))
        subprocess.run(['ffmpeg', '-i', 'concat:' + "|".join(ls_lines), '-acodec', 'copy', overwrite_flag, get_output_name_path()])
    else:
        print((" No mp3-files in (%s) found.") % (source_path))
        exit()


